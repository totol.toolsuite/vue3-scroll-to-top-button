# vue-3-scroll-to-top-button
A Vue.js component for a floating action button (FAB) that scrolls the page to the top.

## Installation
Install the package using npm or yarn:

```bash
npm install vue-3-scroll-to-top-button
# or
yarn add vue-3-scroll-to-top-button
```

## Default example

By default, the button will be on the bottom left, you can play with the options to customize it or positionnate it.

![Demo](https://i.imgur.com/lLIWQC1.gif)

```vue
<script setup lang="ts">
import { ScrollToTopButton } from 'vue-3-scroll-to-top-button';
import 'vue-3-scroll-to-top-button/style.css';
</script>

<template>
  <ScrollToTopButtonVue />
</template>
```


## Example with relative position

You can for example pass props to positionnate relatively the button

![Demo1](https://i.imgur.com/XcdA3rf.gif)

```vue
<script setup lang="ts">
import { ScrollToTopButton } from 'vue-3-scroll-to-top-button';
import 'vue-3-scroll-to-top-button/style.css';
</script>
<template>
  <ScrollToTopButtonVue 
    noTeleport 
    :showButtonTreshold="0" 
    :style="{ position: 'relative' }"
  />
</template>
```

## Example with Custom Icon

Or you can take advantage of the slot to pass a custom icon, or play with the detection treshold

![Demo2](https://i.imgur.com/yKIBbbk.gif)

```vue
<script setup lang="ts">
import { ScrollToTopButton } from 'vue-3-scroll-to-top-button';
import 'vue-3-scroll-to-top-button/style.css';
</script>
<template>
  <div>
  <ScrollToTopButtonVue 
    :showButtonTreshold="300" 
    position="top" 
    class="custom-button" 
    backgroundColor="yellow"
    iconColor="blue"
    >      
      <template #icon>
        <svg viewBox="0 0 24 24" width="48px" height="48px" fill="#ffffff" xmlns="http://www.w3.org/2000/svg">
          <path d="M12 2L3 12h4v8h10v-8h4z" />
        </svg>
      </template>
    </ScrollToTopButtonVue>
  </div>
</template>
<style>
.custom-button {
  height: 100px;
}
</style>
```

## Props

|Name   |Type  | Description  |
|---|---|---|
|anchor  | (string)   | The target element to which the button will be teleported. Defaults to 'body'.  |
|iconColor | 	(string) | The color of the button icon. Defaults to 'white'. |
|backgroundColor  | (string)   | The background color of the button. Defaults to '#2C3436'. |
|showButtonTreshold  | (number)   | The scroll threshold at which the button becomes visible. Defaults to 1. |
|withAnimation  | (boolean)   | Enables or disables the transition animation. Defaults to true. |
|size  | (ButtonSize)   | The size of the button. Can be 'small', 'medium', or 'large'. Defaults to 'medium'. |
|position  | (ButtonPosition)   | The position of the button. Can be 'top-left', 'top', 'top-right', 'right', 'bottom-right', 'bottom', 'bottom-left', or 'left'. Defaults to 'bottom-right'. |
|noTeleport  |  (boolean)   | Disables teleporting the button to a specified anchor. Defaults to false. |
|marginFromSide  |  (string)   | The margin from the side when using noTeleport. Defaults to '12px'. |


## Types

The component is fully written in typescript so you have the best autocompletion
Here are the types of the props so you can manipulate it.

```typescript
export type ButtonSize = 'small' | 'medium' | 'large'

export type ButtonPosition = 'top-left' 
  | 'top' 
  | 'top-right' 
  | 'right' 
  | 'bottom-right'
  | 'bottom'
  | 'bottom-left'
  | 'left'

export interface ScrollToTopButtonProps {
  anchor?: string
  iconColor?: string
  backgroundColor?: string
  showButtonTreshold?: number
  withAnimation?: boolean
  size?: ButtonSize
  position?: ButtonPosition
  noTeleport?: boolean
  marginFromSide?: string
}

```

## Accessibility
The component is designed to be accessible with proper ARIA attributes and roles. The button is given an aria-label of "Scroll to Top" and a role of "button" to enhance accessibility.

## Transitions
The button features a smooth transition effect when appearing or disappearing, enhancing the user experience.

## License
This project is licensed under the MIT License - see the LICENSE file for details.

## Acknowledgments
Thanks to Anatole Acqueberge for creating this amazing Vue 3 confettis plugin! Feel free to contribute, report issues, or suggest improvements. Happy coding! 🎉