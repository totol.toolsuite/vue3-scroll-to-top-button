export type ButtonSize = 'small' | 'medium' | 'large'

export type ButtonPosition = 'top-left' 
  | 'top' 
  | 'top-right' 
  | 'right' 
  | 'bottom-right'
  | 'bottom'
  | 'bottom-left'
  | 'left'

export interface ScrollToTopButtonProps {
  anchor?: string
  iconColor?: string
  backgroundColor?: string
  showButtonTreshold?: number
  withAnimation?: boolean
  size?: ButtonSize
  position?: ButtonPosition
  noTeleport?: boolean
  marginFromSide?: string
}
